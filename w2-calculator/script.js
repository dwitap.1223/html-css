const defaultTheme = Number(localStorage.getItem("theme"));
const toggle1 = document.getElementById("theme1");
const toggle2 = document.getElementById("theme2");
const toggle3 = document.getElementById("theme3");

const root = document.querySelector(":root");

const theme1 = {
  "--bg": "#404464",
  "--textColor": "#ffffff",
  "--bgScreen": "#181c34",
  "--normalButton": "#ece8d8",
  "--colorTextButton": "#282c44",
  "--shadowNormalButton": "#bdb9ae",
  "--bgDelButton": "#68749c",
  "--shadowDelButton": "#464d68",
  "--bgCalButton": "#d44434",
  "--shadowCalButton": "#943025",
  "--textUniqueButton": "#ffffff",
};
const theme2 = {
  "--bg": "#e8e4e4",
  "--textColor": "#34332a",
  "--bgScreen": "#d3cdcd",
  "--normalButton": "#e8e4e4",
  "--colorTextButton": "#34332a",
  "--shadowNormalButton": "#a89d96",
  "--bgDelButton": "#388286",
  "--shadowDelButton": "#215d61",
  "--bgCalButton": "#c85402",
  "--shadowCalButton": "#823a06",
  "--textUniqueButton": "#ffffff",
};
const theme3 = {
  "--bg": "#152636",
  "--textColor": "#b8bec0",
  "--bgScreen": "#3d6176",
  "--normalButton": "#ded9ce",
  "--colorTextButton": "#3d6176",
  "--shadowNormalButton": "#a49d8f",
  "--bgDelButton": "#558d97",
  "--shadowDelButton": "#20484e",
  "--bgCalButton": "#d6b09b",
  "--shadowCalButton": "#977867",
  "--textUniqueButton": "#ffffff",
};

const applyTheme = (theme) =>
  Object.keys(theme).forEach((key) => root.style.setProperty(key, theme[key]));

const setTheme = (theme = 1) => {
  if (theme === 1) {
    applyTheme(theme1);
    localStorage.setItem("theme", 1);
  }
  if (theme === 2) {
    applyTheme(theme2);
    localStorage.setItem("theme", 2);
  }
  if (theme === 3) {
    applyTheme(theme3);
    localStorage.setItem("theme", 3);
  }
};

setTheme(defaultTheme);
